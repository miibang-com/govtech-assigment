import Vue from 'vue'
import Router from 'vue-router'
import i18n from '../lang'

Vue.use(Router);

/* Layout */
import Layout from '../views/layout/Layout'

/**
 * hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
 *                                if not set alwaysShow, only more than one route under the children
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noredirect           if `redirect:noredirect` will no redirct in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar,
  }
 **/
export const constantRouterMap = [
  {path: '/403', name:'403', component: () => import('@/views/403'), hidden: true, redirect: '/403'},
  {path: '/404', name:'404', component: () => import('@/views/404'), hidden: true, redirect: '/404'},
  {
    path: '',
    component: Layout,
    redirect: 'homepage',
    children: [{
      path: 'homepage',
      name: 'Homepage',
      component: () => import('@/views/homepage/index'),
      meta: {title: i18n.t('route.homepage'), icon: 'home'}
    }]
  }
];

export const asyncRouterMap = [
  {path: '*', redirect: '/404', hidden: true}
];


const createRouter = () => new Router({
  mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
});

const router = createRouter();

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter(dynamicRouters) {
  console.log('reset router');
  const newRouter = createRouter();
  router.matcher = newRouter.matcher;// reset router
  router.addRoutes(dynamicRouters);
}

export default router


