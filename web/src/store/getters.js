const getters = {
  sidebar: state => state.app.sidebar,
  language: state => state.app.language,
  device: state => state.app.device,
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
  nickname: state => state.user.nickname,
  roles: state => state.user.roles,
  id: state => state.user.id,
  merchants: state => state.user.merchants,
  permission_routes: state => state.permission.routes,
  errorLogs: state => state.errorLog.logs
};
export default getters
