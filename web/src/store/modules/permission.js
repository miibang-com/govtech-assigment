import { asyncRouterMap, constantRouterMap } from '@/router'

/**
 * Use meta.role to determine if the current user has permission
 * @param roles
 * @param route
 */
function hasPermission(roles, route) {
  if (route.meta && route.meta.roles) {
    return roles.some(role => route.meta.roles.includes(role))
  } else {
    return true
  }
}


function filterAsyncRoutes(routes, roles) {
  const res = [];

  routes.forEach(route => {
    const tmp = { ...route };
    if (hasPermission(roles, tmp)) {
      if (tmp.children) {
        tmp.children = filterAsyncRoutes(tmp.children, roles)
      }
      res.push(tmp)
    }
  });

  return res
}


const permission = {
  // namespaced: true,
  state : {
    routes: [],
    addRoutes: []
  },
  mutations : {
    SET_ROUTES: (state, routes) => {
      state.addRoutes = routes;
      state.routes = constantRouterMap.concat(routes)
    }
  },
  actions: {
    GenerateRoutes({ commit }, roles) {
      return new Promise(resolve => {
        let accessedRoutes;

        // if (roles.includes('ROOT管理员')) {
        //   accessedRoutes = asyncRouterMap || []
        // } else {
        accessedRoutes = filterAsyncRoutes(asyncRouterMap, roles);
        // }
        // console.log(accessedRoutes);
        commit('SET_ROUTES', accessedRoutes);
        resolve(accessedRoutes)
      })
    }
  }
};

export default permission;
