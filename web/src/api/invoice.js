import request from '@/utils/request'


export function fetchDataList(data) {
  return request({
    url:'/invoice/list',
    method:'post',
    data:data
  })
}

