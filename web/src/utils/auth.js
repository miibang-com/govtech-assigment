import Cookies from 'js-cookie'
import {isNotBlank} from "./validate";


const TokenKey = 'loginToken';
const MemberLoginKey = 'memberLoginToken';

export function getToken() {
  console.log("get admin token->");
  return Cookies.get(TokenKey);
}

export function getMemberToken() {
  console.log("get member token->");
  return Cookies.get(MemberLoginKey);
}

export function setToken(token, type) {
  console.log(type);
  if(type === 'member') return Cookies.set(MemberLoginKey, token);
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  Cookies.remove(MemberLoginKey);
  Cookies.remove("robotId");
  Cookies.remove("login_source");
  Cookies.remove("login_username");
  Cookies.remove("login_password");
  Cookies.remove("redirect_page");
  Cookies.remove("product_id");
  Cookies.remove("column_id");
  Cookies.remove("row_id");
  Cookies.remove("roles");
  return Cookies.remove(TokenKey);
}
