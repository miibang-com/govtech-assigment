package com.homework.govtech

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.batch.core.Job
import org.springframework.batch.core.JobParametersBuilder
import org.springframework.batch.core.launch.support.SimpleJobLauncher
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
class BatchTest {
    @Autowired
    var jobLauncher: SimpleJobLauncher? = null

    @Autowired
    var importJob: Job? = null
    @Test
    @Throws(Exception::class)
    fun test() {

        val jobParameters = JobParametersBuilder().addLong("time", System.currentTimeMillis())
                .toJobParameters()
        jobLauncher!!.run(importJob!!, jobParameters)
    }
}