package com.homework.govtech.controller

import com.homework.govtech.common.api.CommonPage
import com.homework.govtech.common.api.CommonResult
import com.homework.govtech.controller.requst.InvoiceQueryParam
import com.homework.govtech.model.Invoice
import com.homework.govtech.service.InvoiceService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*

@Controller
@RequestMapping("/invoice")
class InvoiceController {
    @Autowired
    private val invoiceService: InvoiceService? = null

    @RequestMapping(value = ["/list"], method = [RequestMethod.POST])
    @ResponseBody
    fun list(@RequestBody queryParam: InvoiceQueryParam?): CommonResult<CommonPage<Invoice?>> {
        val invoiceList = invoiceService!!.list(queryParam, queryParam!!.pageSize, queryParam.pageNum)
        return CommonResult.success(CommonPage.restPage(invoiceList))
    }
}