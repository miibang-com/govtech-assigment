package com.homework.govtech.controller

import com.homework.govtech.controller.requst.UploadFile
import com.homework.govtech.controller.response.Message
import com.homework.govtech.service.FileStorageService
import org.springframework.batch.core.Job
import org.springframework.batch.core.JobParametersBuilder
import org.springframework.batch.core.launch.support.SimpleJobLauncher
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.Resource
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder
import java.nio.file.Path
import java.util.stream.Collectors

@RestController
class UploadController {
    @Autowired
    var fileStorageService: FileStorageService? = null

    @Autowired
    var jobLauncher: SimpleJobLauncher? = null

    @Autowired
    var importJob: Job? = null
    @PostMapping("/upload")
    fun upload(@RequestParam("file") file: MultipartFile): ResponseEntity<Message> {
        return try {
            fileStorageService!!.save(file)
            val jobParameters = JobParametersBuilder().addLong("time", System.currentTimeMillis())
                    .toJobParameters()
            jobLauncher!!.run(importJob!!, jobParameters)
            ResponseEntity.ok(Message("Upload file successfully: " + file.originalFilename))
        } catch (e: Exception) {
            ResponseEntity.badRequest()
                    .body(Message("Could not upload the file:" + file.originalFilename))
        }
    }

    @GetMapping("/files")
    fun files(): ResponseEntity<List<UploadFile>> {
        val files = fileStorageService!!.load()
                .map { path: Path ->
                    val fileName = path.fileName.toString()
                    val url = MvcUriComponentsBuilder
                            .fromMethodName(UploadController::class.java,
                                    "getFile",
                                    path.fileName.toString()
                            ).build().toString()
                    UploadFile(fileName, url)
                }.collect(Collectors.toList())
        return ResponseEntity.ok(files)
    }

    @GetMapping("/files/{filename:.+}")
    fun getFile(@PathVariable("filename") filename: String?): ResponseEntity<Resource> {
        val file = fileStorageService!!.load(filename)
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment;filename=\"" + file.filename + "\"")
                .body(file)
    }
}