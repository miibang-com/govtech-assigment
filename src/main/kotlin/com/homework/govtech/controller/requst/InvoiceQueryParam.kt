package com.homework.govtech.controller.requst


import java.math.BigDecimal
import java.util.*


class InvoiceQueryParam {
    val id: Long? = null
    val invoiceNo: String? = null
    val stockCode: String? = null
    val description: String? = null
    val quantity: Int? = null
    val invoiceDate: Date? = null
    val unitPrice: BigDecimal? = null
    val customerId: String? = null
    val country: String? = null
    var pageSize = 5
    var pageNum = 1
}