package com.homework.govtech.common.api

/**
 * 封装API的错误码
 * Created by Charles on  2020/3/19.
 */
interface IErrorCode {
    val code: Long
    val message: String
}