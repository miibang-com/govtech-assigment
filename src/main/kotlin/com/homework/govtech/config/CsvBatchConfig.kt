package com.homework.govtech.config

import com.homework.govtech.component.CsvItemProcessor
import com.homework.govtech.model.Invoice
import lombok.extern.slf4j.Slf4j
import org.springframework.batch.core.Job
import org.springframework.batch.core.Step
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory
import org.springframework.batch.core.launch.support.RunIdIncrementer
import org.springframework.batch.core.launch.support.SimpleJobLauncher
import org.springframework.batch.core.repository.JobRepository
import org.springframework.batch.core.repository.support.JobRepositoryFactoryBean
import org.springframework.batch.item.ItemProcessor
import org.springframework.batch.item.ItemReader
import org.springframework.batch.item.ItemWriter
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider
import org.springframework.batch.item.database.JdbcBatchItemWriter
import org.springframework.batch.item.file.FlatFileItemReader
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper
import org.springframework.batch.item.file.mapping.DefaultLineMapper
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer
import org.springframework.batch.item.validator.Validator
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.io.ClassPathResource
import org.springframework.core.io.PathResource
import org.springframework.transaction.PlatformTransactionManager
import java.nio.file.Paths
import javax.sql.DataSource

@Configuration
@EnableBatchProcessing
@Slf4j
class CsvBatchConfig {

    private val path = Paths.get("fileStorage")

    @Bean
    fun reader(): ItemReader<Invoice> {
        val reader = FlatFileItemReader<Invoice>()
        reader.setResource(PathResource(path.resolve("data.csv").toString()))
        reader.setLinesToSkip(1)
        reader.setLineMapper(object : DefaultLineMapper<Invoice?>() {
            init {
                setLineTokenizer(object : DelimitedLineTokenizer() {
                    init {
                        setNames(*arrayOf("invoiceNo", "stockCode", "description", "quantity", "invoiceDate", "unitPrice", "customerId", "country"))
                    }
                })
                setFieldSetMapper(object : BeanWrapperFieldSetMapper<Invoice?>() {
                    init {
                        setTargetType(Invoice::class.java)
                    }
                })
            }
        })
        return reader
    }

    /**
     * 注册ItemProcessor: 处理数据+校验数据
     * @return
     */
    @Bean
    fun processor(): ItemProcessor<Invoice, Invoice> {
        val cvsItemProcessor = CsvItemProcessor()
        // 设置校验器
        cvsItemProcessor.setValidator(csvBeanValidator())
        return cvsItemProcessor
    }

    /**
     * 注册校验器
     * @return
     */
    @Bean
    fun csvBeanValidator(): Validator<in Invoice> {
        return CsvBeanValidator<Invoice>()
    }

    /**
     * ItemWriter定义：指定datasource，设置批量插入sql语句，写入数据库
     * @param dataSource
     * @return
     */
    @Bean
    fun writer(dataSource: DataSource?): ItemWriter<Invoice> {
        // 使用jdbcBcatchItemWrite写数据到数据库中
        val writer = JdbcBatchItemWriter<Invoice>()
        // 设置有参数的sql语句
        writer.setItemSqlParameterSourceProvider(BeanPropertyItemSqlParameterSourceProvider())
        val sql = "insert into invoice(invoice_no, stock_code, description, quantity, invoice_date, unit_price, customer_id, country) " +
                "values(:invoiceNo,:stockCode,:description,:quantity,:invoiceDate,:unitPrice,:customerId,:country)"
        writer.setSql(sql)
        writer.setDataSource(dataSource!!)
        return writer
    }

    /**
     * JobRepository定义：设置数据库，注册Job容器
     * @param dataSource
     * @param transactionManager
     * @return
     * @throws Exception
     */
    @Bean
    @Throws(Exception::class)
    fun cvsJobRepository(dataSource: DataSource?, transactionManager: PlatformTransactionManager?): JobRepository {
        val jobRepositoryFactoryBean = JobRepositoryFactoryBean()
        jobRepositoryFactoryBean.setDatabaseType("mysql")
        jobRepositoryFactoryBean.transactionManager = transactionManager!!
        jobRepositoryFactoryBean.setDataSource(dataSource!!)
        return jobRepositoryFactoryBean.getObject()
    }

    /**
     * jobLauncher定义：
     * @param dataSource
     * @param transactionManager
     * @return
     * @throws Exception
     */
    @Bean
    @Throws(Exception::class)
    fun csvJobLauncher(dataSource: DataSource?, transactionManager: PlatformTransactionManager?): SimpleJobLauncher {
        val jobLauncher = SimpleJobLauncher()
        // 设置jobRepository
        jobLauncher.setJobRepository(cvsJobRepository(dataSource, transactionManager))
        return jobLauncher
    }

    /**
     * 定义job
     * @param jobs
     * @param step
     * @return
     */
    @Bean
    fun importJob(jobs: JobBuilderFactory, step: Step?): Job {
        return jobs["importCsvJob"]
                .incrementer(RunIdIncrementer())
                .flow(step!!)
                .end()
                .listener(csvJobListener())
                .build()
    }

    /**
     * 注册job监听器
     * @return
     */
    @Bean
    fun csvJobListener(): CsvJobListener {
        return CsvJobListener()
    }

    /**
     * step定义：步骤包括ItemReader->ItemProcessor->ItemWriter 即读取数据->处理校验数据->写入数据
     * @param stepBuilderFactory
     * @param reader
     * @param writer
     * @param processor
     * @return
     */
    @Bean
    fun step(stepBuilderFactory: StepBuilderFactory, reader: ItemReader<Invoice>?,
             writer: ItemWriter<Invoice>?, processor: ItemProcessor<Invoice, Invoice>?): Step {
        return stepBuilderFactory["step"]
                .chunk<Invoice, Invoice>(65000) // Chunk的机制(即每次读取一条数据，再处理一条数据，累积到一定数量后再一次性交给writer进行写入操作)
                .reader(reader!!)
                .processor(processor!!)
                .writer(writer!!)
                .build()
    }
}