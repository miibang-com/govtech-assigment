package com.homework.govtech.config

import org.mybatis.spring.annotation.MapperScan
import org.springframework.context.annotation.Configuration
import org.springframework.transaction.annotation.EnableTransactionManagement

/**
 * MyBatis配置类
 * Created by Charles on  2020/3/8.
 */
@Configuration
@EnableTransactionManagement
@MapperScan("com.homework.govtech.mapper", "com.homework.govtech.dao")
class MyBatisConfig