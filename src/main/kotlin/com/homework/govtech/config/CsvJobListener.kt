package com.homework.govtech.config

import mu.KotlinLogging
import org.springframework.batch.core.JobExecution
import org.springframework.batch.core.JobExecutionListener

private val logger = KotlinLogging.logger {}

class CsvJobListener : JobExecutionListener {
    private var startTime: Long = 0
    private var endTime: Long = 0
    override fun beforeJob(jobExecution: JobExecution) {
        startTime = System.currentTimeMillis()
        logger.info("job process start...")
    }

    override fun afterJob(jobExecution: JobExecution) {
        endTime = System.currentTimeMillis()
        logger.info("job process end...")
        logger.info("elapsed time: " + (endTime - startTime) + "ms")
    }
}