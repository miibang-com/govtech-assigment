package com.homework.govtech.config

import org.springframework.batch.item.validator.ValidationException
import org.springframework.batch.item.validator.Validator
import org.springframework.beans.factory.InitializingBean
import javax.validation.Validation

class CsvBeanValidator<T> : Validator<T>, InitializingBean {
    private var validator: javax.validation.Validator? = null

    /**
     * 进行JSR-303的Validator的初始化
     * @throws Exception
     */
    @Throws(Exception::class)
    override fun afterPropertiesSet() {
        val validatorFactory = Validation.buildDefaultValidatorFactory()
        validator = validatorFactory.usingContext().validator
    }

    /**
     * 使用validator方法检验数据
     * @param value
     * @throws ValidationException
     */
    @Throws(ValidationException::class)
    override fun validate(value: T) {
        val constraintViolations = validator!!.validate(value)
        if (constraintViolations.size > 0) {
            val message = StringBuilder()
            for (constraintViolation in constraintViolations) {
                message.append("""
    ${constraintViolation.message}

    """.trimIndent())
            }
            throw ValidationException(message.toString())
        }
    }
}