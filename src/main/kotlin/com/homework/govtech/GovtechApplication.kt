package com.homework.govtech

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class GovtechApplication

fun main(args: Array<String>) {
	runApplication<GovtechApplication>(*args)
}
