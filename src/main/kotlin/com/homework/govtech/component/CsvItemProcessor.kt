package com.homework.govtech.component

import cn.hutool.core.util.StrUtil
import com.homework.govtech.model.Invoice
import org.springframework.batch.item.validator.ValidatingItemProcessor
import org.springframework.batch.item.validator.ValidationException
import mu.KotlinLogging
private val logger = KotlinLogging.logger {}

class CsvItemProcessor : ValidatingItemProcessor<Invoice>() {
    @Throws(ValidationException::class)
    override fun process(item: Invoice): Invoice {
        // 执行super.process()才能调用自定义的校验器
        logger.info("processor start validating...")
        super.process(item)

        // 数据处理，比如将中文性别设置为M/F
        if (StrUtil.isAllEmpty(item.invoiceNo)) {
            item.invoiceNo = "N.A"
        }
        logger.info("processor end validating...")
        return item
    }
}