package com.homework.govtech.service

import com.homework.govtech.controller.requst.InvoiceQueryParam
import com.homework.govtech.model.Invoice

interface InvoiceService {
    fun list(queryParam: InvoiceQueryParam?, pageSize: Int?, pageNum: Int?): List<Invoice?>?
}