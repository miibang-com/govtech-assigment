package com.homework.govtech.service.impl

import com.homework.govtech.service.FileStorageService
import org.springframework.core.io.Resource
import org.springframework.core.io.UrlResource
import org.springframework.stereotype.Service
import org.springframework.util.FileSystemUtils
import org.springframework.web.multipart.MultipartFile
import java.io.IOException
import java.net.MalformedURLException
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.stream.Stream

@Service
class FileStorageServiceImpl : FileStorageService {
    private val path = Paths.get("fileStorage")
    override fun init() {
        try {
            if(!Files.exists(path)) Files.createDirectory(path)
        } catch (e: IOException) {
            throw RuntimeException("Could not initialize folder for upload!")
        }
    }

    override fun save(multipartFile: MultipartFile) {
        try {
            if(Files.exists(path.resolve(multipartFile.originalFilename))) Files.delete(path.resolve(multipartFile.originalFilename))
            Files.copy(multipartFile.inputStream, path.resolve(multipartFile.originalFilename))
        } catch (e: IOException) {
            throw RuntimeException("Could not store the file. Error:" + e.message)
        }
    }

    override fun load(filename: String): Resource {
        val file = path.resolve(filename)
        return try {
            val resource: Resource = UrlResource(file.toUri())
            if (resource.exists() || resource.isReadable) {
                resource
            } else {
                throw RuntimeException("Could not read the file.")
            }
        } catch (e: MalformedURLException) {
            throw RuntimeException("Error:" + e.message)
        }
    }

    override fun load(): Stream<Path> {
        return try {
            Files.walk(path, 1)
                    .filter { path: Path -> path != this.path }
                    .map { other: Path? -> path.relativize(other) }
        } catch (e: IOException) {
            throw RuntimeException("Could not load the files.")
        }
    }

    override fun clear() {
        FileSystemUtils.deleteRecursively(path.toFile())
    }
}