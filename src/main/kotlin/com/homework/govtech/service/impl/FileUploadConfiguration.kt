package com.homework.govtech.service.impl

import com.homework.govtech.service.FileStorageService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Service

@Service
class FileUploadConfiguration : CommandLineRunner {
    @Autowired
    var fileStorageService: FileStorageService? = null
    @Throws(Exception::class)
    override fun run(vararg args: String) {
//        fileStorageService.clear();
        fileStorageService!!.init()
    }
}