package com.homework.govtech.service.impl

import cn.hutool.core.util.StrUtil
import com.github.pagehelper.PageHelper
import com.homework.govtech.controller.requst.InvoiceQueryParam
import com.homework.govtech.dao.InvoiceDao
import com.homework.govtech.mapper.InvoiceMapper
import com.homework.govtech.model.Invoice
import com.homework.govtech.model.InvoiceExample
import com.homework.govtech.service.InvoiceService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class InvoiceServiceImpl : InvoiceService {
    @Autowired
    private val invoiceMapper: InvoiceMapper? = null
    override fun list(queryParam: InvoiceQueryParam?, pageSize: Int?, pageNum: Int?): List<Invoice?>? {
        PageHelper.startPage<Any>(pageNum!!, pageSize!!)
        val invoiceExample = InvoiceExample()
        val criteria:InvoiceExample.Criteria = invoiceExample.createCriteria()
        if(!StrUtil.isAllBlank(queryParam!!.customerId)) criteria.andCustomerIdEqualTo(queryParam.customerId)
        if(!StrUtil.isAllBlank(queryParam.invoiceNo)) criteria.andInvoiceNoEqualTo(queryParam.invoiceNo)
        if(!StrUtil.isAllBlank(queryParam.stockCode)) criteria.andStockCodeEqualTo(queryParam.stockCode)
        if(!StrUtil.isAllBlank(queryParam.country)) criteria.andCountryEqualTo(queryParam.country)
        return invoiceMapper!!.selectByExample(invoiceExample)
    }
}