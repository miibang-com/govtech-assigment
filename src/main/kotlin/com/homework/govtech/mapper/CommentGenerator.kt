package com.homework.govtech.mapper

import org.mybatis.generator.api.IntrospectedColumn
import org.mybatis.generator.api.IntrospectedTable
import org.mybatis.generator.api.dom.java.CompilationUnit
import org.mybatis.generator.api.dom.java.Field
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType
import org.mybatis.generator.internal.DefaultCommentGenerator
import org.mybatis.generator.internal.util.StringUtility
import java.util.*

/**
 * 自定义注释生成器
 * Created by Charles on  2020/3/26.
 */
class CommentGenerator : DefaultCommentGenerator() {
    private var addRemarkComments = false

    /**
     * 设置用户配置的参数
     */
    override fun addConfigurationProperties(properties: Properties) {
        super.addConfigurationProperties(properties)
        this.addRemarkComments = StringUtility.isTrue(properties.getProperty("addRemarkComments"))
    }

    /**
     * 给字段添加注释
     */
    override fun addFieldComment(field: Field, introspectedTable: IntrospectedTable,
                                 introspectedColumn: IntrospectedColumn) {
        var remarks = introspectedColumn.remarks
        //根据参数和备注信息判断是否添加备注信息
        if (addRemarkComments && StringUtility.stringHasValue(remarks)) {
//            addFieldJavaDoc(field, remarks);
            //数据库中特殊字符需要转义
            if (remarks.contains("\"")) {
                remarks = remarks.replace("\"", "'")
            }
            //给model的字段添加swagger注解
            field.addJavaDocLine("@ApiModelProperty(value = \"$remarks\")")
        }
    }

    /**
     * 给model的字段添加注释
     */
    private fun addFieldJavaDoc(field: Field, remarks: String) {
        //文档注释开始
        field.addJavaDocLine("/**")
        //获取数据库字段的备注信息
        val remarkLines = remarks.split(System.getProperty("line.separator").toRegex()).toTypedArray()
        for (remarkLine in remarkLines) {
            field.addJavaDocLine(" * $remarkLine")
        }
        addJavadocTag(field, false)
        field.addJavaDocLine(" */")
    }

    override fun addJavaFileComment(compilationUnit: CompilationUnit) {
        super.addJavaFileComment(compilationUnit)
        //只在model中添加swagger注解类的导入
        if (!compilationUnit.isJavaInterface && !compilationUnit.type.fullyQualifiedName.contains(EXAMPLE_SUFFIX)) {
            compilationUnit.addImportedType(FullyQualifiedJavaType(API_MODEL_PROPERTY_FULL_CLASS_NAME))
        }
    }

    companion object {
        private const val EXAMPLE_SUFFIX = "Example"
        private const val API_MODEL_PROPERTY_FULL_CLASS_NAME = "io.swagger.annotations.ApiModelProperty"
    }
}