package com.homework.govtech.mapper

import org.mybatis.generator.api.MyBatisGenerator
import org.mybatis.generator.config.xml.ConfigurationParser
import org.mybatis.generator.internal.DefaultShellCallback
import java.util.*

/**
 * 用于生产MBG的代码
 * Created by Charles on  2020/3/26.
 */
object Generator {
    @Throws(Exception::class)
    @JvmStatic
    fun main(args: Array<String>) {
        //MBG 执行过程中的警告信息
        val warnings: List<String> = ArrayList()
        //当生成的代码重复时，覆盖原代码
        val overwrite = true
        //读取我们的 MBG 配置文件
        val `is` = Generator::class.java.getResourceAsStream("/generatorConfig.xml")
        val cp = ConfigurationParser(warnings)
        val config = cp.parseConfiguration(`is`)
        `is`.close()
        val callback = DefaultShellCallback(overwrite)
        //创建 MBG
        val myBatisGenerator = MyBatisGenerator(config, callback, warnings)
        //执行生成代码
        myBatisGenerator.generate(null)
        //输出警告信息
        for (warning in warnings) {
            println(warning)
        }
    }
}