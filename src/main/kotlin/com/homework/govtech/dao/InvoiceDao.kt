package com.homework.govtech.dao

import com.homework.govtech.controller.requst.InvoiceQueryParam
import com.homework.govtech.model.Invoice
import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import org.springframework.stereotype.Repository

@Repository
@Mapper
interface InvoiceDao {
    fun getList(@Param("queryParam") queryParam: InvoiceQueryParam?): List<Invoice?>?
}