# Getting Started

##preparation
1, please create a database called: homework

2, import the sql file:  db/schema.sql

##Unit test
please put data.csv under fileStorage/, then run BatchTest.

##start backend
1, main class is: com.homework.govtech.GovtechApplication.kt

2, just run it

##start frontend

1, go to folder: web/

2, npm install

3, npm run dev

4, access http://localhost:8090/homepage

##PS
Sorry for the mess comment in the code, I need to finish the project quickly because busy working schedule.

There are lot of points can be optimized, sorry for the bugs.


